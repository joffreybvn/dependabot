# frozen_string_literal: true

class Version
  VERSION = "0.32.0"
end
